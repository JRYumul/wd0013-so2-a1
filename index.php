<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Activity</title>
	<!-- FONT -->
	<link href="https://fonts.googleapis.com/css?family=Eagle+Lake&display=swap" rel="stylesheet"> 
	<!-- BOOTSTRAP CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- EXTERNAL CSS -->
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>
<body>
	<section class="container mt-5">
		<h1 class="border-bottom text-center pb-3 mb-3">What's Your Star Sign?</h1>
		<div class="form-group">
			<h4 class="pb-1">Enter your birthday:</h4>
			<form method="POST" action="index_action.php">
				Month: <input class="form-control" type="number" name="month" required><br>
				Day: <input class="form-control" type="number" name="day" required><br>
				<button class="btn btn-primary" type="submit">Submit</button>
			</form>
		</div>
	</section>
</body>
</html>