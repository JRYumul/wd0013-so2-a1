<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Activity</title>
	<!-- FONT -->
	<link href="https://fonts.googleapis.com/css?family=Eagle+Lake&display=swap" rel="stylesheet"> 
	<!-- BOOTSTRAP CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- EXTERNAL CSS -->
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>
<body>
	<section class="container">
		<div class="text-center big-p">
			<?php

				$month = $_POST["month"];
				$day = $_POST["day"];

				get_sign($month, $day);

				function get_sign($month, $day){

					if($month == "1"){
						if(($day >= "1") && ($day <= "19")){
							echo "<h1>You're a Capricorn!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "20") && ($day <= "31")){
							echo "<h1>You're an Aquarius!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "2"){
						if(($day >= "1") && ($day <= "18")){
							echo "<h1>You're an Aquarius!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "19") && ($day <= "29")){
							echo "<h1>You're a Pisces!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "3"){
						if(($day >= "1") && ($day <= "20")){
							echo "<h1>You're a Pisces!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "21") && ($day <= "31")){
							echo "<h1>You're an Aries!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "4"){
						if(($day >= "1") && ($day <= "19")){
							echo "<h1>You're an Aries!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "20") && ($day <= "30")){
							echo "<h1>You're a Taurus!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "5"){
						if(($day >= "1") && ($day <= "20")){
							echo "<h1>You're a Taurus!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "21") && ($day <= "31")){
							echo "<h1>You're a Gemini!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "6"){
						if(($day >= "1") && ($day <= "20")){
							echo "<h1>You're a Gemini!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "21") && ($day <= "30")){
							echo "<h1>You're a Cancer!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "7"){
						if(($day >= "1") && ($day <= "22")){
							echo "<h1>You're a Cancer!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "23") && ($day <= "31")){
							echo "<h1>You're a Leo!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "8"){
						if(($day >= "1") && ($day <= "22")){
							echo "<h1>You're a Leo!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "23") && ($day <= "31")){
							echo "<h1>You're a Virgo!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "9"){
						if(($day >= "1") && ($day <= "22")){
							echo "<h1>You're a Virgo!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "23") && ($day <= "30")){
							echo "<h1>You're a Libra!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "10"){
						if(($day >= "1") && ($day <= "22")){
							echo "<h1>You're a Libra!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "23") && ($day <= "31")){
							echo "<h1>You're a Scorpio!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "11"){
						if(($day >= "1") && ($day <= "21")){
							echo "<h1>You're a Scorpio!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "23") && ($day <= "31")){
							echo "<h1>You're a Sagittarius!</h1>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month == "12"){
						if(($day >= "1") && ($day <= "21")){
							echo "<h1>You're a Sagittarius!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else if(($day >= "22") && ($day <= "31")){
							echo "<h1>You're a Capricorn!</h1>";
							echo "<small>Wow! Amazing!</small>";
						}else{
							echo "<h2>Error: Invalid Date</h2>";
						}
					}
					if($month < "1" || $month > "12"){
						echo "<h2>Error: Invalid Date</h2>";
					}
					};

				echo '<br>';
				echo '<br>';
				echo '<a href="index.php">Go back</a>';

			?>
		</div>
	</section>
</body>
</html>